import React from "react";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";

import pages from "./components/pages";
import sections from "./components/pages/sections";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <div className="main-conteiner">
          {sections.Header()}
          <div className="content" style={{minHeight: window.innerWidth/3 > 200 ? window.innerWidth/3 : 200 }}>
            <Route path="/series"> {pages.Series()} </Route>
            <Route path="/movies"> {pages.Movies()}</Route>
            <Route path="/error"> {pages.Error()}</Route>
            <Route path="/loading"> {pages.Loading()}</Route>
            <Route exact path="/">
              {pages.Home()}
            </Route>
          </div>

          {sections.Footer()}
        </div>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
