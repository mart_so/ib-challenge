import React from "react";
import Miniature from "./../Miniature";

function MiniatureContainer(entries) {
  const listItems = entries.map((entrie) => (
    <React.Fragment key={entrie.title}>{Miniature(entrie)}</React.Fragment>
  ));

  return <div className="miniature-container"> {listItems} </div>;
}

export default MiniatureContainer;
