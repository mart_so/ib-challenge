import React from "react";

import sections from "./sections";
import MiniatureContainer from "./../containers/MiniatureContainer";
import db from "./../../mock-db.json";
import util from "./../../utils/";

function Series() {
  
  let series = util.filtrateList(db.entries, "series");
  console.log(series);

  return (
    <React.Fragment>
      {sections.Title({ title: "Popular Series" })}
      {MiniatureContainer(series)}
    </React.Fragment>
  );
}

export default Series;
