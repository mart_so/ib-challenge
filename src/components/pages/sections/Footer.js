import React from "react";
import appStore from "./../../../assets/images/store/app-store.svg";
import playStore from "./../../../assets/images/store/play-store.svg";
import windowStore from "./../../../assets/images/store/windows-store.svg";
import instagram from "./../../../assets/images/social/instagram-white.svg";
import facebook from "./../../../assets/images/social/facebook-white.svg";
import twitter from "./../../../assets/images/social/twitter-white.svg";

function Footer() {
  return (
    <footer>
      <div className="container">
        <div className="links">
          <p>Home</p>
          <div className="divider-vertical"></div>
          <p> Terms and Conditions</p>
          <div className="divider-vertical"></div>
          <p>Privacy Policy</p>
          <div className="divider-vertical"></div>
          <p>Collection Statement</p>
          <div className="divider-vertical"></div>
          <p>Help</p>
          <div className="divider-vertical"></div>
          <p> Manager Account</p>
          <div className="divider-vertical"></div>
        </div>

        <div className="copyright" >
          <p>Copyright © 2016 DEMO Streaming. All Rights Reserved.</p>
        </div>

        <div className="images">
          <div className="social">
            <img src={facebook} alt="Facebook" />
            <img src={twitter} alt="Twitter" />
            <img src={instagram} alt="Instagram" />
          </div>
          <div className="appStore">
            <img src={appStore} alt="App Store" />
            <img src={playStore} alt="Play Store" />
            <img src={windowStore} alt="Window Store" />
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
