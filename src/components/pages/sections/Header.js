import React from "react";

function Header() {
  return (
    <div className="header">
      <a className="link" href="/">
        <div className="logo">
          <h1>DEMO Streaming</h1>
        </div>
      </a>
      <div className="nav">
        <a href="/error">
          <div className="login">
            <p> Log in</p>
          </div>
        </a>
        <a href="/error">
          <div className="trial">
            <button>Start your free trial</button>
          </div>
        </a>
      </div>
    </div>
  );
}

export default Header;
