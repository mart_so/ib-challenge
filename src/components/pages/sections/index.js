import Header from "./Header";
import Footer from "./Footer";
import Title from "./Title";

export default {
  Header,
  Footer,
  Title,
};
