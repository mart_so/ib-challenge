import Home from "./Home";
import Series from "./Series";
import Movies from "./Movies";
import Error from "./Error";
import Loading from "./Loading";

export default {
  Home,
  Series,
  Movies,
  Error,
  Loading
};
