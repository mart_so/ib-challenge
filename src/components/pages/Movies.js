import React from "react";

import sections from "./sections";
import MiniatureContainer from "./../containers/MiniatureContainer";
import db from "./../../mock-db.json";
import util from "./../../utils/";

function Movies() {
  
  let movies = util.filtrateList(db.entries, "movie");
  console.log(movies);

  return (
    <React.Fragment>
      {sections.Title({ title: "Popular Movies" })}
      {MiniatureContainer(movies)}
    </React.Fragment>
  );
}

export default Movies;
