import React from "react";

function Error() {
  return (
    <div className="error-container">
    <div className="error">
        <p>Oops, something went wrong...</p>
      </div>
    </div>
  );
}

export default Error;
