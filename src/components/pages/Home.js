import React from "react";
import sections from "./sections";
import Miniature from "./../Miniature";

function Home() {
  return (
    <React.Fragment>
      {sections.Title({ title: "Popular Titles" })}
      <div className="miniature-container">
        {Miniature(null, "Series")} {Miniature(null, "Movies")}
      </div>
    </React.Fragment>
  );
}

export default Home;
