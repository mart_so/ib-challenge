import React from "react";
import defaultImage from "./../assets/images/placeholder.png";

function Miniature(entrie, category) {
  const image = entrie ? entrie.images["Poster Art"] : null;

  return (
    <div className="miniature">
      <div className={entrie ? "miniature-image" : "miniature-category"}>
        {entrie ? (
          <img className="fillwidth fillheight" src={image.url} alt="" />
        ) : (
          <a className="link" href={category.toLowerCase()}>
            <img src={defaultImage} alt="" />
            <p className="category-name">{category}</p>
          </a>
        )}
      </div>

      {entrie ? (
        <div className=".miniature.title">{entrie.title}</div>
      ) : (
        <div className=".miniature.title">Popular {category}</div>
      )}
    </div>
  );
}

export default Miniature;
