export default {
  filtrateList: (entitie, category) => {
    let movies = entitie;

    movies = movies.filter((entrie) => {
      return entrie.programType === category;
    });

    movies = movies.filter((entrie) => {
      return entrie.releaseYear >= 2010;
    });

    movies.sort((a, b) => {
      if (a.title > b.title) {
        return 1;
      }
      if (a.title < b.title) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

    movies.slice(0, 21);

    return movies;
  },
};
