1- How did you decide on the technical and architectural choices used as part of your solution?
3- Are there any improvements you could make to your submission?
3- What would you do differently if you were allocated more time?

1-The app is basically a very simple app, so I did not consider using context or redux for state management or even hooks.

For routing, I used react-router-dom, as using the apps internal state alone would not be efficient enough.

I ensured I made functional components and very reusable.

2- Clean the code. Improve some imports. Implement a real API REST for get the data and implement a redux-saga solution for manage these change in the SPA and  so implement a real error catcher and a real "feaching" functionality to show the loading screen

3- Write css in diferent files and use a precompiler like sass.
Install eslint for keep a perfect code.
Write comments in some non trivial bloques of lines.
Write better description in git commits.
Improve some styles.
Add more features for the navegation.

## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm run start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!